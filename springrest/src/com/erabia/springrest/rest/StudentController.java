package com.erabia.springrest.rest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erabia.springrest.bean.Student;
import com.erabia.springrest.exception.StudentException;
import com.erabia.springrest.service.StudentService;
@RestController
@RequestMapping("/api")
public class StudentController {
	private StudentService studentService;

	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	public  StudentService getStudentService() {
		return studentService;
	}

	public  void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}
	@PostMapping("/students")
	public Student addStudent(@RequestBody Student student) {
		return studentService.add(student);
	}
	
	@GetMapping("/students")
	public List<Student> getAll() {
		return studentService.getAll().get();
	}
	@GetMapping("/students/test")
	public List<Student> getAll2() {
		List<Student> li = new LinkedList<Student>();
		return li;
	}
	@GetMapping("/students/{id}")
	public Student getStudentById(@PathVariable int id) throws StudentException {
		return studentService.get(id).get();
	}
	@PutMapping("/students")
	public Student updateStudent(@RequestBody Student student) {
		return studentService.update(student);
	}
	@DeleteMapping("/students/{id}")
	public void deleteStudent(@PathVariable int id) throws StudentException {
		 studentService.delete(id);;
	}
	
}
