package com.erabia.springrest.exception;

import com.erabia.springrest.exception.enums.StudentExceptionType;

public class StudentException  extends Exception{
	private final StudentExceptionType type;
	private final String value;
	public StudentException(final StudentExceptionType type, final String message,final String value) {
		super(message);
		this.type = type;
		this.value = value;
	}
	public StudentExceptionType getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	
	

}
