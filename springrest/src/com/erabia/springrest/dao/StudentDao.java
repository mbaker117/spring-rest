package com.erabia.springrest.dao;

import java.util.List;
import java.util.Optional;

import com.erabia.springrest.bean.Student;
import com.erabia.springrest.exception.StudentException;

public interface StudentDao {
	public Student add(Student student);
	public Student update(Student student);
	public void delete(int id) throws StudentException;
	public Optional<Student> get(int id);
	public Optional<List<Student>> getAll();
}
