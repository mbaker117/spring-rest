package com.erabia.springrest.dao.impl;

import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.erabia.springrest.bean.Student;
import com.erabia.springrest.dao.StudentDao;
import com.erabia.springrest.exception.StudentException;
import com.erabia.springrest.exception.enums.StudentExceptionType;
@Repository
public class StudentDaoImpl implements StudentDao {
	private SessionFactory sessionFactory;

	public StudentDaoImpl(SessionFactory sessionFactory) {

		this.sessionFactory = sessionFactory;
	}

	
	public StudentDaoImpl() {
		
		// TODO Auto-generated constructor stub
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Student add(Student student) {
		// TODO Auto-generated method stub
		if (student == null)
			throw new IllegalArgumentException("student is null");
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		session.close();
		return student;
	}

	@Override
	public Student update(Student student) {
		// TODO Auto-generated method stub
		if (student == null)
			throw new IllegalArgumentException("student is null");
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(student);
		session.getTransaction().commit();
		session.close();
		return student;
	}

	@Override
	public void delete(int id) throws StudentException {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Optional<Student> optional = get(id);
		if (!optional.isPresent())
			throw new StudentException(StudentExceptionType.NO_EXISTING_STUDENT,
					StudentExceptionType.NO_EXISTING_STUDENT.getMsg(), null);
		session.delete(optional.get());
		session.getTransaction().commit();
		session.close();

	}

	@Override
	public Optional<Student> get(int id) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Student student = session.get(Student.class, id);
		session.getTransaction().commit();
		session.close();
		return Optional.ofNullable(student);
	}

	@Override
	public Optional<List<Student>> getAll() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query<Student> query = session.createQuery("from Student");

		return Optional.ofNullable(query.getResultList());
	}

}
