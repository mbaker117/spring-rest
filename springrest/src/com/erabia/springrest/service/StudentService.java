package com.erabia.springrest.service;

import java.util.List;
import java.util.Optional;

import com.erabia.springrest.bean.Student;
import com.erabia.springrest.exception.StudentException;



public interface StudentService {
	public Student add(Student std) ;

	public Student update( Student std);

	public void delete(int id) throws StudentException;

	public Optional<Student> get(int id) throws StudentException;

	public Optional<List<Student>> getAll() ;
}
