package com.erabia.springrest.service.impl;


import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.erabia.springrest.bean.Student;
import com.erabia.springrest.dao.StudentDao;
import com.erabia.springrest.exception.StudentException;
import com.erabia.springrest.service.StudentService;



@Service
public class StudentServiceImpl implements StudentService {
	private StudentDao studentDao;

	public StudentServiceImpl(StudentDao studentDAO) throws StudentException {
		this.studentDao = studentDAO;
	}

	public Student add(Student std) {
		// TODO Auto-generated method stub
		
		return studentDao.add(std);
		
	}

	public Student update(Student std) {
		// TODO Auto-generated method stub
		return studentDao.update(std);
	}

	public void delete(int id) throws StudentException {
		// TODO Auto-generated method stub
		studentDao.delete(id);
	}

	public Optional<Student> get(int id) throws StudentException {
		// TODO Auto-generated method stub
		return studentDao.get(id);
	}

	public Optional<List<Student>> getAll() {
		// TODO Auto-generated method stub
		return studentDao.getAll();
	}

	

}
