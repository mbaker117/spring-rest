package com.erabia.springrestclient.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.erabia.springrestclient.bean.Student;
import com.erabia.springrestclient.exception.StudentException;
import com.erabia.springrestclient.service.StudentService;
@Service
public class StudentServiceImpl implements StudentService {
	private static final String URL="http://localhost:8888/springrest/api/students";
	@Override
	public Student add(Student std) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		Student result = restTemplate.postForObject(URL, std, Student.class);
		return result;
	}

	@Override
	public Student update(Student std) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		 restTemplate.put(URL, std);
		 return std;
		
	}

	@Override
	public void delete(int id) throws StudentException {
		// TODO Auto-generated method stub
		final String DELETE_URL=URL+"/{id}";
		Map<String,Integer> params = new HashMap<>();
		params.put("id",id);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(DELETE_URL,params);
	
	}

	@Override
	public Student get(int id) throws StudentException {
		// TODO Auto-generated method stub
		final String GET_URL=URL+"/{id}";
		Map<String,Integer> params = new HashMap<>();
		params.put("id",id);
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(GET_URL,Student.class,params);
	}

	@Override
	public List<Student> getAll() {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		List<Student> students =(List<Student>) restTemplate.getForObject(URL, List.class);
		return students;
	}

}
