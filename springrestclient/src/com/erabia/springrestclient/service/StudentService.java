package com.erabia.springrestclient.service;

import java.util.List;
import java.util.Optional;

import com.erabia.springrestclient.bean.Student;
import com.erabia.springrestclient.exception.StudentException;

public interface StudentService {
	public Student add(Student std);

	public Student update(Student std);

	public void delete(int id) throws StudentException;

	public Student get(int id) throws StudentException;

	public List<Student> getAll();
}
