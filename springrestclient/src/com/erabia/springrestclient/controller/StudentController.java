package com.erabia.springrestclient.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.erabia.springrestclient.bean.Student;
import com.erabia.springrestclient.exception.StudentException;
import com.erabia.springrestclient.service.StudentService;

@Controller
@RequestMapping("/student")
public class StudentController {
	private StudentService studentService;

	public StudentController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}

	public final StudentService getStudentService() {
		return studentService;
	}

	public final void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}

	@GetMapping("/viewStudent")
	public String getStudents(Model model) {
		List<Student> students = studentService.getAll();
		model.addAttribute("students", students);
		return "studentPage";
	}

	/*
	 * @GetMapping("/getSudent/{id}") public String
	 * getStudents(@RequestParam("studentId") int id,Model model) throws
	 * StudentException { Student student = studentService.get(id);
	 * model.addAttribute("student",student); return "studentPage"; }
	 */
	@GetMapping("/editStudent")
	public String editStudent(@RequestParam("studentId") int id, Model model) throws StudentException {
		Student student = studentService.get(id);
		model.addAttribute("student", student);
		return "studentForm";
	}

	@GetMapping("/deleteStudent")
	public String editStudent(@RequestParam("studentId") int id) throws StudentException {
		studentService.delete(id);
		return "redirect:/student/viewStudent";
	}

	@GetMapping("/addStudentForm")
	public String addStudentForm(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		return "studentForm";
	}

	@PostMapping("/saveStudent")
	public String saveStudent(@Valid @ModelAttribute Student student, BindingResult br) {
		if (br.hasErrors())
			return "studentForm";
		if (student.getId() == 0)
			studentService.add(student);
		else
			studentService.update(student);
		return "redirect:/student/viewStudent";
	}
	@InitBinder 
	public void validate(WebDataBinder webDataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}

}
