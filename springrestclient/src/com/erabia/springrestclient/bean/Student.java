package com.erabia.springrestclient.bean;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Student {
	
	private int id;
	@NotNull
	@Size(min=1,message="last name cannot be less than 1 character")
	private String firstName;

	@NotNull
	@Size(min=1,message="last name cannot be less than 1 character")
	private String lastName;
	
	@NotNull
	@Min(value=0,message="avg cannot be less than zero")
	@Max(value=100,message = "avg cannot be more than 100")
	private double avg;

	public Student() {
	}

	public Student(String firstName, String lastName, double avg) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.avg = avg;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", avg=" + avg + "]";
	}

}
